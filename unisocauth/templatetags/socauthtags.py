from django import template

register = template.Library()


@register.inclusion_tag('unisocauth/_auth_box.html', takes_context=True)
def login_box(context, **kwargs):
    context.update(kwargs)
    return context
