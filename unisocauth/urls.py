from django.conf.urls import patterns, include, url
from django.contrib.auth.views import logout_then_login
from unisocauth import views


urlpatterns = patterns('',
    url(r'^$', views.login_page),
    url(r'^signup-email/', views.signup_email),
    url(r'^email-sent/', views.validation_sent),
    url(r'^login/$', views.login_page),
    url(r'^associated/$', views.associated_page, name='assocs_list'),
    url(r'^email/$', views.require_email, name='require_email'),
    url(r'^logout/$', logout_then_login, name='logout'),
    url(r'', include('social.apps.django_app.urls', namespace='social'))
)
