from distutils.core import setup

setup(
    name='unisocauth',
    version='0.1',
    packages=['unisocauth', 'unisocauth.templatetags'],
    install_requires=['python-social-auth'],
    url='',
    license='',
    author='DmZio',
    author_email='',
    description='Django app for internal usage (simplifies python-social-auth setup)'
)
